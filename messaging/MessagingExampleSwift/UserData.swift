//
//  UserData.swift
//  EmergencyCorridoriOSLibrary
//
//  Created by danielhilpoltsteiner on 21.08.17.
//  Copyright © 2017 Daniel Hilpoltsteiner. All rights reserved.
//

import Foundation
import EVReflection

public class UserData : EVObject {
    public var userLocations = [NSDictionary]()
    
    required public init(){}

    public init(userLocations: [NSDictionary] ){
        self.userLocations = userLocations;
    }

    public func getUserLocations() -> [NSDictionary] {
        return userLocations
    }
}

