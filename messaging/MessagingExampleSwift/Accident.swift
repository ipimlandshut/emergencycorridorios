//
//  Accident.swift
//  Class represents an DataObject which is transmitted within a Push Notification
//  Reflection based object instantiation is ensured with EVReflection
//
//  Created by danielhilpoltsteiner on 05.09.17.
//  Copyright © 2017 Google Inc. All rights reserved.
//

import Foundation
import EVReflection
@objc
public class Accident : EVObject {
    public var id : Int = -1;
    public var street : String = "";
    public var timeInMilliSec : Int64 = -1
    public var accidentLocation : GPXEntry = GPXEntry()
    public var trafficJamEndLocation : GPXEntry = GPXEntry()
    public var exitNearbyAccident : String = "";
    public var exitNearbyJamEnd : String = ""
    
    /*
     Does exactly what the name implies: Calculating the Distance between the accidentLocation and the TrafficJamLocation
     */
    public func getDistanceBetweenAccidentAndEndOfTrafficJam() -> Double {
        return accidentLocation.distanceTo(locations: trafficJamEndLocation)
    }
}
