
//
//  MapMatchingService.swift
//  Communicates with the Tomcat Server by transmitting current gps positions.
//
//  Created by danielhilpoltsteiner on 17.08.17.
//  Copyright © 2017 Google Inc. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON
import JustLog

public class MapMatchingService {
    
    // Returns a Streetname from an Array of GPS Positions
    public static func getStreetFromGPSPosition(gpsLocations: [NSDictionary]) -> Dictionary<String, Any> {
        let semaphore = DispatchSemaphore(value: 0)
        let postURL = URL(string: Constants.URI + "/getStreet")
        print(gpsLocations)

        var request = URLRequest(url: postURL!);
        request.httpMethod = "POST";
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type");
        request.httpBody = try! JSONSerialization.data(withJSONObject: gpsLocations);
        
        var result : Dictionary<String, Any> = Dictionary();
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if(error != nil ) {
                print("Error \(String(describing: error))")
                return
            }
            do {
                print(data!)
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! Dictionary<String, Any>
                print(jsonResult);
                result = jsonResult;
                Logger.shared.info("getStreetFromGPSPosition \(result)");
            } catch let error {
                print(error)
                result = Dictionary<String, Any>()
            }
            
            semaphore.signal();
        }
        task.resume()
        semaphore.wait()
        return result
    }
}
