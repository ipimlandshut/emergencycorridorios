//
//  Copyright (c) 2016 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import FirebaseMessaging
import JustLog

@objc(ViewController)
class ViewController: UIViewController {
    let delegate = AppDelegate();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
    }
    
    @IBAction func handleLogTokenTouch(_ sender: UIButton) {
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
    }
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBAction func handleSubscribeTouch(_ sender: UISwitch) {
        let topicName : String = "EmergencyCorridor";
        Logger.shared.info("UISwitch state : \(sender.isOn) ")
        if sender.isOn {
            Messaging.messaging().subscribe(toTopic: topicName)
        } else {
            Messaging.messaging().unsubscribe(fromTopic: topicName)
            delegate.resetRegions();
        }
    }
}
