//
//  Copyright (c) 2016 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import UserNotifications
import AVFoundation
import Foundation
import Firebase
import FirebaseMessaging
import SwiftLocation
import CoreLocation
import JustLog

/**
 Delegate class to handle GPS and Notifications
 **/
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let debug = false;
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let locationManager = CLLocationManager();
    var count = 0 ;
    var lastDictionaryEntries = [NSDictionary]()
    var accidentList : [Accident] = [Accident]()
    
    //Setting up the Logger on logz.io. There are options to self-host or logging the data locally
    func setupLogger(){
        let logger = Logger.shared
        logger.logFilename = "demo.log"
        //logger.logstashHost = "my.logstash.endpoint.com"
        //logger.logstashPort = 3515
        logger.logstashHost = "listener.logz.io"
        logger.logstashPort = 5052
        logger.logzioToken = "NeDRuCHOLyTncGgeQJZvDLssSflzfQfc"
        logger.logstashTimeout = 5
        logger.logLogstashSocketActivity = true
        
        // default info
        logger.defaultUserInfo = ["app": "my iOS App",
                                  "environment": "production",
                                  "tenant": "UK",
                                  "sessionID": "sessionid"]
        
        logger.enableConsoleLogging = true
        logger.enableFileLogging = true
        logger.enableLogstashLogging = true
        logger.setup()
    }
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupLogger();
        UIApplication.shared.registerForRemoteNotifications()
        FirebaseApp.configure()
        initializeLocationManager();
        // as of iOS 11 we need to set additional
        //NSLocationAlwaysAndWhenInUseUsageDescription String
        
        Locator.requestAuthorizationIfNeeded(.always);
        //testCLRegion();
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        if let _ = launchOptions?[UIApplicationLaunchOptionsKey.location] {
        
            Locator.subscribeSignificantLocations(onUpdate: { newLocation in
                Logger.shared.info("subscribeSignificantLocations onUpdate: got Details of significant location \(newLocation)" )
                // This block will be executed with the details of the significant location change that triggered the background app launch,
                // and will continue to execute for any future significant location change events as well (unless canceled).
                
                //Maybehere we kcan use the currentLocation and invoke if request was given
            }, onFail: { (err, lastLocation) in
                Logger.shared.info("subscribeSignificantLocations onFail: with \(err) \(lastLocation)" )
                
                // Something bad has occurred
            })
        }
        Logger.shared.info("didFinishLaunchingWithOptions");
        
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        return true
    }
    
    // Gets called if a remote message is received.
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        Logger.shared.info("didReceiveRemoteNotification UIApplication userInfo");
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        print("userinfo \(userInfo)")
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if (alert["message"] as? NSString) != nil {
                    let json = try? JSONSerialization.jsonObject(with: alert["message"] as! Data, options: []) as! [String:Any]
                    let message = json?["body"] as! String
                    print(message)
                    if #available(iOS 10, *) {
                        speak(text: message)
                    } else {
                        // Fallback on earlier versions
                    }
                    //Do stuff
                }
            } else if (aps["alert"] as? NSString) != nil {
                //Do stuff
            }
        }
        
        
        // Print full message.
        print(userInfo)
    }
    
    func onSubscribePositionSuccess(location : CLLocation) {
        if(self.count < Constants.SKIP_POSITIONS) {
            self.count += 1;
        } else {
            let entry = GPXEntry(location: location).toDictionary()
            lastDictionaryEntries.insert(entry, at: 0)
            if(lastDictionaryEntries.count == Constants.AMOUNT_OF_POSITIONS) {
                Logger.shared.info("locationManager didUpdateLocatioself.ns AMOUNT_OF_POSITIONS: \(lastDictionaryEntries.count), \(lastDictionaryEntries)");
                self.count += 1;
                checkIfUserIsRelevantForAccident(locations: lastDictionaryEntries, accidents: accidentList)
                Locator.completeAllLocationRequests()
                lastDictionaryEntries.removeAll();
                
            }
        }
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        Logger.shared.info("didReceiveRemoteNotification fetchCompletionHandler")
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
    
        checkIfExistingRegionsAreOutdated()
        print(" Received Information from APN: \(userInfo)")
        
        Locator.subscribePosition(accuracy: .room, onUpdate: { (location) -> (Void) in
            self.onSubscribePositionSuccess(location: location);
        }, onFail: {
            err, lastLocation in
            Logger.shared.info("Locator.subscribeSignificantLocations onFail: \(err, lastLocation)")
        })
        accidentList = [Accident](json: (userInfo["accidentList"] as! String))
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // Resets all saved regions for the application
    // This needs to be done, because iOS apps can only hold about 20 regions at the same time
    // Adding Regions does not work well after the set of regions is over 20 items.
    public func resetRegions() {
        Logger.shared.info("resetRegions :\(locationManager.monitoredRegions)")
        for region in locationManager.monitoredRegions {
                locationManager.stopMonitoring(for: region);
        }
        Logger.shared.info("resetRegions stopped :\(locationManager.monitoredRegions)")
    }
    
    // Initializing the LocationManager at start allows to check for authorizationstatus of the app (userposition)
    public func initializeLocationManager() -> Void {
        Logger.shared.info("initializeLocationManager")
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        if #available(iOS 9.0, *) {
            locationManager.allowsBackgroundLocationUpdates = true
            Logger.shared.info("allowsBackgroundLocationUpdates == true : \(locationManager.allowsBackgroundLocationUpdates == true)")
        } else {
            // Fallback on earlier versions
        }
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization();
    }
    

    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Logger.shared.warning("didFailToRegisterForRemoteNotificationsWithError \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Logger.shared.info("didRegisterForRemoteNotificationsWithDeviceToken");
        print("APNs token retrieved: \(deviceToken)")
        var readableToken: String = ""
        for i in 0..<deviceToken.count {
            readableToken += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("Received an APNs device token: \(readableToken)")
        // With swizzling disabled you must set the APNs token here.
        Messaging.messaging().apnsToken = deviceToken
        let topic = "EmergencyCorridor"
        Messaging.messaging().subscribe(toTopic: topic)
            print("Subscribed to topic: \(topic)")
    }
}

// Code for handling actions on UserNotification starting here
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        Logger.shared.info("Request: \(notification.request) \(notification.request.content)")
        let content = notification.request.content
        let text = content.body + content.title
        speak(text: text);
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        Logger.shared.info("userNotificationCenter didReceive response");
        let userInfo = response.notification.request.content.userInfo
        let content = response.notification.request.content
        let text = content.body + content.title
        speak(text: text);
        if let messageID = userInfo[gcmMessageIDKey] {
            Logger.shared.info("userNotificationCenter Message ID: \(messageID)")
        }
        
        completionHandler()
    }
    
    
     //Method uses AVSpeeUtterance to turn given Text into audio voice
    
    func speak(text : String) {
        Logger.shared.info("speak notification");
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: "de-DE");
        let synth = AVSpeechSynthesizer()
        synth.speak(utterance)
    }
    
    // Extracts the text from an dictionary
    func extractAndSpeak(dict : NSDictionary){
        print("extractAndSpeak \(dict)")
        let exit0: String = dict["title"] as! String
        let exit1: String  = dict["body"] as! String
        let notificationString = "Es wurde erkannt, dass Sie zwischen Anschlussstelle \(exit0) und \(exit1) im Stau stehen. Grund dafür ist ein Unfall. Bitte bilden Sie eine Rettungsgasse."
        
        speak(text: notificationString)
    }
}


extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        Logger.shared.info("Firebase registration token: \(fcmToken)")
    }
    
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        Logger.shared.info("MessagingDelegate messaging \(remoteMessage)");
    }
}

// Code for handling regions using LocationManager starts here
extension AppDelegate : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        Logger.shared.info("locationManager didExitRegion: \(region.identifier)");
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        Logger.shared.info("locationManager didEnterRegion: \(region.identifier)");
        let accident = Accident(json: region.identifier);
        let gpxEntry: GPXEntry = GPXEntry(location: manager.location!);
        checkIfUserIsRelevantForAccident(locations: [gpxEntry.toDictionary()], accidents: [accident])
        notifyUser(accident: accident)
        manager.stopMonitoring(for: region);
    }
    
    // most important function at the moment: calls  to get information if the user should get notified
    func checkIfUserIsRelevantForAccident(locations: [NSDictionary], accidents: [Accident]) {
        let userData = UserData(userLocations: locations);
        for accident in self.accidentList {
            let isUserRelevantForAccident = AlertService.isUserRelevantForAccident(accident: accident, userData: userData);
            Logger.shared.info("isUserRelevantForAccident \(isUserRelevantForAccident, accident.street)");
            // Uncomment for Testing purpose if(true) {
            if(isUserRelevantForAccident) {
                notifyUser(accident: accident)
            } else {
                registerGeofence(accident: accident);
            }
        }
    }
    
    // generates an local notification which is displayed if the user is nearby the accident
    private func notifyUser(accident: Accident) {
        Logger.shared.info("notifyUser \(accident)");
        if #available(iOS 10.0, *) {
            let titleString = "Rettungsgasse zwischen Anschlussstelle \(accident.exitNearbyAccident) und \(accident.exitNearbyJamEnd) bilden";
            let bodyString = "Es wurde erkannt, dass Sie in einem Stau stehen. "
            let content = UNMutableNotificationContent()
            content.title = NSString.localizedUserNotificationString(forKey: titleString, arguments: nil)
            content.body = NSString.localizedUserNotificationString(forKey: bodyString, arguments: nil)
            content.sound = UNNotificationSound.default()
            content.categoryIdentifier = "notify-test"
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request);
        } else {
            // Fallback on earlier versions
        };
    }
    
    // is user is not near the received accident we create a new geofence
    private func registerGeofence(accident: Accident) {
        Logger.shared.info("willRegisterGeofence: \(accident.street)");
        let authorizationStatus = CLLocationManager.authorizationStatus();
        
        if (authorizationStatus != .authorizedWhenInUse && authorizationStatus != .authorizedAlways){
            // User has not authorized access to location information.
            return
        }
        // Do not start services that aren't available.
        if !CLLocationManager.locationServicesEnabled() {
            // Location services is not available.
            return
        }
        if(!CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self)) {
            return
        }
        
        print(locationManager.monitoredRegions.count, locationManager.monitoredRegions)
        if(locationManager.monitoredRegions.count >= 20) {
            let region = locationManager.monitoredRegions.first
            locationManager.stopMonitoring(for: region!)
        }
        let region = CLCircularRegion(center: accident.accidentLocation.getLocation().coordinate, radius: Constants.GEOFENCE_RADIUS_IN_METERS, identifier: accident.toJsonString())
        locationManager.startMonitoring(for: region);
        Logger.shared.info("didRegisterGeofenceForRegion \(accident.street, locationManager.monitoredRegions.count, locationManager.monitoredRegions)");
    }
    
    // helper method to check if the timestamp of old regions exceed a specific timelimit
    private func checkIfExistingRegionsAreOutdated() {
        for region in locationManager.monitoredRegions {
            let accident : Accident = Accident(json: region.identifier);
            let currentTimeInMilliSec :Int64 = Int64(Date().timeIntervalSince1970 * 1000);
            if(currentTimeInMilliSec - accident.timeInMilliSec > 24*60*60*1000) {
                locationManager.stopMonitoring(for: region);
            }
        }
    }
}


