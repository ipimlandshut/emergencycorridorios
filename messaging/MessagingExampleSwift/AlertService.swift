//
//  AlertService.swift
//  Class handles the recognition if a user is relevant for a certain accident by using the method isUserRelevantForAccident
//
//  Created by danielhilpoltsteiner on 21.08.17.
//  Copyright © 2017 Daniel Hilpoltsteiner. All rights reserved.
//

import Foundation
import JustLog
public class AlertService {
    
    // checks if user is relevant for an accident, using the other class methods
    public static func isUserRelevantForAccident(accident : Accident, userData: UserData) -> Bool {
        Logger.shared.info("isUserRelevantForAccident \(accident.street)");
        let userLocations : [NSDictionary] = userData.getUserLocations();
        let isUserCloseToAccident: Bool = isUserCloseToAccidentArea(accident: accident, userLocation: GPXEntry(dictionary: userLocations[userLocations.count - 1 ]) )
        let isUserOnSameStreet : Bool = isUserOnSameStreetAsAccident(accident: accident, userLocations: userLocations)
        let isUserApproachingFromRight : Bool = isUserApproachingFromTheRightDirection(accident: accident, userData: userData);
        print("isUserCloseToAccident: \(isUserCloseToAccident), isUserOnSameStreet: \(isUserOnSameStreet), isUserApproachingFromRightDirection: \(isUserApproachingFromRight)")
        return (isUserCloseToAccident && isUserOnSameStreet && isUserApproachingFromRight)
            ?  true :  false;
    }
    
    // Checks if the User is close to an accident, by calculating the distance using gps positions
    private static func isUserCloseToAccidentArea(accident: Accident, userLocation : GPXEntry) -> Bool {
        let accidentLocation : GPXEntry = accident.accidentLocation;
        let distanceToAccident : Double = userLocation.distanceTo(locations: accidentLocation)
        let distanceBetweenAccidentAndEndOfTrafficJam = accident.getDistanceBetweenAccidentAndEndOfTrafficJam()
        return distanceBetweenAccidentAndEndOfTrafficJam + Constants.WARNING_AREA_AFTER_TRAFFIC_JAM > distanceToAccident;
    }
    
    // checks if the current streetname provided based on gps position matches the given on from the accident
    private static func isUserOnSameStreetAsAccident( accident : Accident, userLocations : [NSDictionary]) -> Bool {
        var accidentLocations = [GPXEntry]();
        accidentLocations.append(accident.accidentLocation)
        accidentLocations.append(accident.trafficJamEndLocation)
        let accidentStreet : String = accident.street;
        let streetObject = MapMatchingService.getStreetFromGPSPosition(gpsLocations: userLocations)
        var result = false;
        if(!streetObject.isEmpty) {
            let userStreet = streetObject["streetName"] as! String!
            print(userStreet!, accidentStreet, userStreet!.contains(accidentStreet))
            result = userStreet!.contains(accidentStreet);
        }
        return result
    }
    
    // using the gps position, we can find the direction in which the user is driving. e.g. is the driver driving towards the accident
    private static func isUserApproachingFromTheRightDirection(accident: Accident, userData : UserData) -> Bool {
        let userLocations : [NSDictionary] = userData.getUserLocations();
        
        let usersLastKnownLocation : GPXEntry = GPXEntry(dictionary: userLocations[userLocations.count - 1]);
        let usersFirstKnownLocation : GPXEntry = GPXEntry(dictionary:userLocations[0]);
        let usersPenultimateKnownLocation : GPXEntry = GPXEntry(dictionary:userLocations[userLocations.count - 2]);
        
        let lastKnownDistanceUserToAccident : Double = (accident.accidentLocation.distanceTo(locations: usersLastKnownLocation));
        let lastKnownDistanceUserToEndOfTrafficJam : Double = (accident.trafficJamEndLocation.distanceTo(locations: usersLastKnownLocation));
        let firstKnownDistanceToAccident : Double = (accident.accidentLocation.distanceTo(locations: usersFirstKnownLocation)) ;
        let penultimateKnownDistanceUserToAccident : Double = (accident.accidentLocation.distanceTo(locations: usersPenultimateKnownLocation));
        let penultimateKnownDistanceUserToEndOfTrafficJam : Double = (accident.trafficJamEndLocation.distanceTo(locations: usersPenultimateKnownLocation));
        
        if(lastKnownDistanceUserToAccident > lastKnownDistanceUserToEndOfTrafficJam && firstKnownDistanceToAccident > lastKnownDistanceUserToAccident){
            return true;
        } else if(penultimateKnownDistanceUserToEndOfTrafficJam < lastKnownDistanceUserToEndOfTrafficJam && penultimateKnownDistanceUserToAccident > lastKnownDistanceUserToAccident){
            return true;
        }
        return false;
    }
}

