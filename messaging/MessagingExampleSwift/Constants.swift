//
//  File.swift
//  Constant Class which defines some values for the algorithms
//
//  Created by danielhilpoltsteiner on 05.09.17.
//  Copyright © 2017 Google Inc. All rights reserved.
//

import Foundation

public class Constants {
    
    public static let MIN_TIME : Int = 1000;
    public static let minDistance : Double = 0;
    public static let BASE_URI = "http://rgasse.bayerninfo.de:2880"
    public static let URI : String = BASE_URI + "/EmergencyCorridorServer/rest/services";
    public static let PACKAGE_NAME : String = "de.hawla.ipim.emergencycorridorlibrary.model";
    public static let WARNING_AREA_AFTER_TRAFFIC_JAM : Double = 500.0;
    public static let GEOFENCE_EXPIRATION_IN_HOURS : Int = 3;
    public static let GEOFENCE_RADIUS_IN_METERS : Double = 50;
    public static let GCM_MESSAGE_ID_KEY = "gcm.message_id"
    public static let AMOUNT_OF_POSITIONS = 5
    public static let SKIP_POSITIONS = 2
};
