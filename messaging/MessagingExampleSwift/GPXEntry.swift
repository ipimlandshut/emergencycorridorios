//
//  GPXEntry.swift
//  Class represents an DataObject which wraps informations about gps positions
//  Reflection based object instantiation is ensured with EVReflection
//
//  Created by danielhilpoltsteiner on 18.08.17.
//  Copyright © 2017 Google Inc. All rights reserved.
//

import Foundation
import CoreLocation
import EVReflection


public class GPXEntry: EVObject {
    var latitude : Double = 0.0;
    var longitude : Double = 0.0;//    var location : CLLocation;
    var timeInMilliSec : Int = 0;
    
    public func getLocation() -> CLLocation {
        let location = CLLocation(latitude: self.latitude, longitude: self.longitude);
        return location;
    }
    
    public func distanceTo(locations other: GPXEntry) -> Double {
        let ownLocation = self.getLocation();
        let otherLocation = other.getLocation()
        return ownLocation.distance(from: otherLocation)
    }
    
    public init(location: CLLocation) {
        //        self.location = location;
        self.latitude = location.coordinate.latitude;
        self.longitude = location.coordinate.longitude
        self.timeInMilliSec = Int(location.timestamp.timeIntervalSince1970)
    }
    
    required convenience public init?(coder: NSCoder) {
        self.init()
//        fatalError("init(coder:) has not been implemented")
    }
    
    required public init() {
        super.init();
    }

}
