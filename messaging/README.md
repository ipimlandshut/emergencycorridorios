Rettungsgasse iOS Anwendung
=============================
Willkommen zur Code-Basis der iOS Anwendung für die Rettungsgassen Applikation. 

Die Anwendung informiert Nutzer über die Bildung einer Rettungsgasse falls dies erforderlich ist. Dazu kommuniziert die Anwendung über Google Cloud Messaging mit einer Google Firebase Instanz. 
Die K


Wichtige Anmerkungen 
=============================

Es gibt Probleme bei der Nutzung von Firebase in einer separaten App unabhängigen Bibliothek. Nach Aussagen ist dies nicht gewollt. Es wird darüber gesprochen, dieses Verhalten zu ändern. Allerdings ist dies zum Zeitpunkt der Entwicklung noch nicht der Fall.
https://github.com/firebase/quickstart-ios/issues/245
(Zitat)
There are other limitations here; Firebase can't (shouldn't) be used as a dependency of a dylib since it's distributed as a static library. We are talking about changing this, but as the saying goes, I can't give you a timeline or anything concrete.


https://github.com/CocoaPods/CocoaPods/issues/3276
(Zitat)
No, this is not supported. You can only set custom sources for a dependency inside a Podfile, not a podspec.

https://github.com/CocoaPods/CocoaPods/issues/6138
(Zitat)
It looks like the Firebase pod does have a module map in it, but the podspec for Firebase isn't properly setup to use the module map using the podspec dsl here: https://guides.cocoapods.org/syntax/podspec.html#module_map.
(Zitat)
Until the module map is properly setup for Firebase, you won't be able to have statements like import Firebase. Please contact the pod author (appears to be Google) to have this fixed. Thanks!

Es kann zu Problemen beim Empfang von Silent Push Notifications kommen. Da diese scheinbar vom iOS System unter bestimmten Umständen geschluckt werden. 
https://stackoverflow.com/questions/44796613/silent-pushes-not-delivered-to-the-app-on-ios-11 

Getting Started
---------------

- Add APNS certs to your project in **Project Settings** > **Notifications** in the [console](https://console.firebase.google.com)
- Run `pod install`
- Copy in the GoogleServices-Info.plist to your project
- Update the app Bundle ID in Xcode to match the Bundle ID of your APNs cert.
- Run the sample on your iOS device.

Note:
- You will need Swift 3.0 to run the Swift version of this quickstart.
- APS Environment Entitlements are required for remote notifications as of Xcode 8.
  Ensure that Push Notifications are on without error in App > Capabilities.
